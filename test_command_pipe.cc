#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CLOSE_ALL()                                                            \
  do {                                                                         \
    close(fifo[0]);                                                            \
    close(fifo[1]);                                                            \
  } while (0);

int main(int argc, char **argv) {
  int fifo[2];
  pid_t pid1, pid2;

  if (pipe(fifo)) {
    perror("pipe");
    _exit(2); /* create a command pipe */
  }

  switch (pid1 = fork()) { /* execute command to the left of pipe */
    case -1:
      perror("fork");
      exit(3);
    case 0:
      if (dup2(fifo[1], STDOUT_FILENO) == -1) {
        perror("dup2");
        exit(4);
      }
      CLOSE_ALL();
      if (execlp("/bin/sh", "sh", "-c", argv[1], 0) == -1) {
        perror("execl");
        exit(5);
      }
  }

  switch (pid2 = fork()) { /* execute command to the right of pipe */
    case -1:
      perror("fork");
      exit(6);
    case 0:
      if (dup2(fifo[0], STDIN_FILENO) == -1) {
        perror("dup2");
        exit(7);
      }
      CLOSE_ALL();
      if (execlp("/bin/sh", "sh", "-c", argv[2], 0) == -1) {
        perror("execl");
        exit(8);
      }
  }

  CLOSE_ALL();
  
  if (waitpid(pid1, &fifo[0], 0) != pid1 || waitpid(pid2, &fifo[1], 0) != pid2)
    perror("waitpid");
  
  return fifo[0] + fifo[1];
}
