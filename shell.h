/* This is the shell.h header file, which declares the CMD_INFO class */

#ifndef SHELL_H
#define SHELL_H

#include <iostream>
#include <string.h>
#include <assert.h>
#include <malloc.h>

using std::cout;
using std::cerr;
using std::endl;

/* check IO redirection and command pipe conflict */
#define CHECK_IO(fn)                                                           \
  do {                                                                         \
    if (fn) {                                                                  \
      cerr << "Invalid re-direct: " << fn << endl;                             \
      delete fn;                                                               \
      fn = 0;                                                                  \
    }                                                                          \
  } while (0);

class CMD_INFO {

public:
  char **argv;   // command and argument list
  char *infile;  // std input re-directed file
  char *outfile; // std. output re-directed file
  int backgrnd;  // 1 if cmd to run in backgrnd

  CMD_INFO *pSubcmd; // cmds to be run in a sub-shell
  CMD_INFO *Pipe;    // next command after `|'
  CMD_INFO *Next;    // next command after `;'

  /* Ctor */
  CMD_INFO() : argv(0), backgrnd(0) {
    infile = outfile = 0;
    pSubcmd = Pipe = Next = 0;
  }

  /* Dtor */
  ~CMD_INFO() {
    delete infile;
    delete outfile;
    for (int i = 0; argv && argv[i]; i++) {
      delete argv[i];
    }
    delete argv;
  }

  // Add one argument string to argv list
  void add_arg(char *str) {
    int len = 1;
    if (!argv) {
      argv = (char **)malloc(sizeof(char *) * 2);
    } else {
      while (argv[len])
        len++;
      len++;
      argv = (char **)realloc(argv, sizeof(char *) * (len + 1));
    }
    assert(argv[len - 1] = strdup(str));
    argv[len] = 0;
  }

  // Add a standard input or output redirect file name
  void add_iofile(char *&iofile, char *fnm) {
    if (iofile) {
      cerr << "Multiple in-direct: " << iofile << " vs " << fnm << endl;
    } else {
      iofile = fnm;
    }
  }

  // Add a command pipe
  void add_pipe(CMD_INFO *pCmd) {
    if (Pipe)
      Pipe->add_pipe(pCmd);
    else {
      CHECK_IO(outfile);
      CHECK_IO(pCmd->infile);
      Pipe = pCmd;
    }
  }

  // Add a next command stage
  void add_next(CMD_INFO *pCmd) {
    if (Next) {
      Next->add_next(pCmd);
    } else {
      Next = pCmd;
    }
  }
};

extern void exec_cmd(CMD_INFO *pCmd);

extern "C" {
  int yyparse(void);
  int yylex(void);
}

#endif
