/* shell.l: mini-shell lexical analyzer lex soruce file */

%{

#include <string.h>
#include "shell.h"
#include "y.tab.h"

/* scanner wrap up routine */
extern "C" int yywrap()  { return 1; }

%}

%%

;[ \t\v]*\n    return '\n';        /* skip `;' at end of line */
^[ \t\v]*\n               ;        /* skip blank lines */
^#[^\n]\n                 ;        /* skip comment lines */
#[^\n]*                   ;        /* skip in-line comment s*/
[ \t\v]                   ;        /* skip white space */
[A-Za-z_0-9/,.-]+  {
              yylval.s = strdup(yytext);
              return NAME;         /* return a character token */
            }  
.           |                      /* a single char token */
\n          return yytext[0];

%%

