/* shell.y: The mini-shell program parser */

%{

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include "shell.h"

using namespace std;

static CMD_INFO * pCmd = 0;

/* parser error reporting routine */
void yyerror(const char *s) {
  fprintf(stderr, "%s\n", s);
}

/* add a cmd or arg to vector list */
CMD_INFO *add_vect (CMD_INFO *pCmd, char *str) {
  int len = 1;
  if (!pCmd) assert(pCmd = new CMD_INFO);
  pCmd->add_arg(str);
  return pCmd;
}

%}

%union 
{
  char*    s;
  CMD_INFO*  c;
}

%token <s> NAME

%%

input_stream : cmd_line '\n'
               {   exec_cmd($<c>1);           }
      |  input_stream cmd_line   '\n'
               {   exec_cmd($<c>2);           }
      |   error '\n'
               {   yyerrok; yyclearin;        }
      ;

cmd_line : shell backgrnd
               {   $<c>$ = $<c>1;             }
      |  cmd_line       ';'   shell backgrnd
               {   $<c>1->add_next($<c>3);
                 $<c>$ = $<c>1;
               }
      ;

shell : basic
               {   $<c>$ = $<c>1;             }
      |  complex
               {   $<c>$ = $<c>1;             }
      |  shell     '|'   basic
               {   $<c>1->add_pipe($<c>3);
                 $<c>$ = $<c>1;
               }
      |  shell     '|'   complex
               {   $<c>1->add_pipe($<c>3);
                 $<c>$ = $<c>1;
               }
      ;  

basic :  cmd_spec io_spec
               {   $<c>$ = $<c>1;             }
      ;

complex      :  '('   cmd_line       ')' 
               {   pCmd = new CMD_INFO;
                 pCmd->pSubcmd = $<c>2;
               }
         io_spec
               {   $<c>$ = pCmd;              }
      ;

cmd_spec :  NAME
               {   $<c>$ = pCmd = add_vect(0,$<s>1);  }
      |  cmd_spec NAME
               {   $<c>$ = add_vect($<c>1,$<s>2);     }
      ;

io_spec :  /* empty */
      |  io_spec redir
      ;

redir :  '<'   NAME
               {   pCmd->add_iofile(pCmd->infile, $<s>2);   }
      |  '>'   NAME
               {   pCmd->add_iofile(pCmd->outfile,$<s>2);   }
      ;

backgrnd :  /* empty */
      |  '&'
              {   pCmd->backgrnd = 1;   }
      ;

%%

