#include <iostream>
#include <stdio.h>
#include <unistd.h>

using std::cout;
using std::endl;

int main() {

  switch (fork()) {
    case (pid_t)-1: perror("fork"); break;        /* fork fails */
    case (pid_t) 0: cout << "Child process created" << endl; return 0;
    default       : cout << "Parent process after fork" << endl; break;
  }

  return 0;
}
