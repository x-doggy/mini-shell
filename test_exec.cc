#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// emulate the C 'system' function
int System(const char *cmd) {

  pid_t        pid;
  int        status;

  switch (pid = fork()) {
    case -1: return -1;
    case 0:
      execl("/bin/sh", "sh", "-c", cmd, 0);
      perror("execl");
      exit(errno);
  }

  if (waitpid(pid, &status, 0) == pid && WIFEXITED(status)) {
    return WEXITSTATUS(status);
  }    

  return -1;
}

int main() {

  int rc = 0;
  char buf[256];

  do {
    printf("sh> ");
    fflush(stdout);
    if (!fgets(buf, 256, stdin)) break;
    rc = System(buf);
  } while (!rc);

  return rc;
}
