# Mini-shell

Mini-shell able to launch commands written in C++, Linux API, Flex and Yacc.

** This is some project placed somewhere in [cse.csusb.edu](cse.csusb.edu) **

This Directory contains the example programs as described in Chapter 8:

| Section | Program |
| ------- | ------- |
| 8.2.1   | test_fork.c |
| 8.2.2   | test_exit.c |
| 8.2.3   | test_waitpid.c |
| 8.2.4   | test_exec.c |
| 8.2.5   | test_pipe.c |
| 8.2.6.2 | command_pipe.c, command_pipe2.c |
| 8.2.6.3 | test_popen.c |
| 8.3     | getproc.c |
| 8.5     | shell.y, shell.l, shell.h, shell.c, exec_cmd.c |


## Build mini-shell

Just type:

```bash
make
```

You also can build some test programs described in the table below from 8.2.1 to 8.3. Type:

```bash
make testcases
```

To clean build type:

```bash
make clean
```

