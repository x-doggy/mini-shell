#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace std;

int main() {
  pid_t child_pid, pid;
  int status;
  switch (child_pid = fork()) {
    case (pid_t)-1:
      perror("fork"); /* fork fails */
      break;
    case (pid_t)0:
      cout << "Child process created\n";
      _exit(15); /* terminate child */
    default:
      cout << "Parent process after fork\n";
      pid = waitpid(child_pid, &status, WUNTRACED);
  }

  if (WIFEXITED(status))
    cerr << child_pid << " exits: " << WEXITSTATUS(status) << endl;
  else if (WIFSTOPPED(status))
    cerr << child_pid << " stopped by: " << WSTOPSIG(status) << endl;
  else if (WIFSIGNALED(status))
    cerr << child_pid << " killed by: " << WTERMSIG(status) << endl;
  else
    perror("waitpid");

  _exit(0);
}
