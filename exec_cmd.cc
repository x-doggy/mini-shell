/* exec_cmd: Functions to execute one shell command input line */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include "shell.h"

using std::cout;
using std::cerr;
using std::endl;

/* change the standard I/O port of a process */
void chg_io(char *fileName, mode_t mode, int fdesc) {
  int fd = open(fileName, mode, 0777);
  if (fd == -1)
    perror("open");
  else {
    if (dup2(fd, fdesc) == -1)
      perror("dup2");
    close(fd);
  }
}

/* execute one or more command pipe */
void exec_pipes(CMD_INFO *pCmd) {
  CMD_INFO *ptr;
  int fifo[2][2];
  int bg = 0, first = 1, cur_pipe = 0;
  pid_t pid;
  while (ptr = pCmd) {
    pCmd = ptr->Pipe;
    if (pipe(fifo[cur_pipe]) == -1) {
      perror("pipe");
      return;
    }
    switch (fork()) {
    case -1:
      perror("fork");
      return;
    case 0:
      if (!first) { // not the first cmd
        dup2(fifo[1 - cur_pipe][0], 0);
        close(fifo[1 - cur_pipe][0]);
      } else if (ptr->infile)
        chg_io(ptr->infile, O_RDONLY, 0);
      if (pCmd) // not the last cmd
        dup2(fifo[cur_pipe][1], 1);
      else if (ptr->outfile)
        chg_io(ptr->outfile, O_WRONLY | O_CREAT | O_TRUNC, 1);
      close(fifo[cur_pipe][0]);
      close(fifo[cur_pipe][1]);
      execvp(ptr->argv[0], ptr->argv);
      cerr << "Execute `" << ptr->argv[0] << "` fails" << endl;
      exit(4);
    }
    if (!first)
      close(fifo[1 - cur_pipe][0]);
    close(fifo[cur_pipe][1]);
    cur_pipe = 1 - cur_pipe;
    bg = ptr->backgrnd;
    delete ptr;
    first = 0;
  }
  close(fifo[1 - cur_pipe][0]);
  while (!bg && (pid = waitpid(-1, 0, 0)) != -1);
}

/* execute one shell command line */
void exec_cmd(CMD_INFO *pCmd) {
  pid_t prim_pid, pid;
  CMD_INFO *ptr = pCmd;

  // create a sub-shell to process one command line
  switch (prim_pid = fork()) {
  case -1:
    perror("fork");
    return;
  case 0:
    break;
  default:
    if (waitpid(prim_pid, 0, 0) != prim_pid)
      perror("waitpid");
    return;
  }
  while (ptr = pCmd) { // execute each command stage
    pCmd = ptr->Next;
    if (ptr->Pipe)
      exec_pipes(ptr); // execute one stage which has command pipes
    else {
      // sub-process to execute one command stage
      switch (pid = fork()) {
      case -1:
        perror("fork");
        return;
      case 0:
        break;
      default:
        if (!ptr->backgrnd && waitpid(pid, 0, 0) != pid)
          perror("waitpid");
        delete ptr;
        continue;
      }
      if (ptr->infile)
        chg_io(ptr->infile, O_RDONLY, 0);
      if (ptr->outfile)
        chg_io(ptr->outfile, O_WRONLY | O_CREAT | O_TRUNC, 1);
      if (ptr->argv) {
        execvp(ptr->argv[0], ptr->argv);
        cerr << "Execute `" << ptr->argv[0] << "` fails" << endl;
        exit(999);
      } else {
        exec_cmd(ptr->pSubcmd);
        exit(0);
      }
    }
  }
  exit(0);
}
