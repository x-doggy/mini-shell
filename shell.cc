/* shell: The mini-shell main program */

#include <iostream>
#include <stdio.h>

extern "C" int yyparse();
extern "C" FILE *yyin;

using std::cerr;
using std::endl;

int main(int argc, char **argv) { 

  if (argc > 1) {
    while (--argc > 0) {
      if (yyin = fopen(*++argv, "r")) {
        yyparse();
      } else {
        cerr << "Can't open file: " << *argv << endl;
      }
    }
  } else {
    yyparse();
  } 

  return 0;
}
