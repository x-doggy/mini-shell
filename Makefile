CXX = g++
LEX = flex
YACC = yacc
RM = rm

testcases = test_fork test_exit test_waitpid      \
            test_exec test_pipe test_popen        \
            test_command_pipe                     \
            test_command_pipe2                    \
            test_getproc                          \

shellc = shell.y.o shell.yy.o shell.o exec_cmd.o

all: mini-shell

.SECONDEXPANSION:

testcases: $(testcases)

$(testcases): %: %.cc
	$(CXX) -o $@ $@.cc

mini-shell: $(shellc)
	$(CXX) -o $@ $(shellc)

shell.y.o: shell.y
	$(YACC) -d shell.y
	$(CXX) -c y.tab.c -o shell.y.o
	$(RM) -f y.tab.c

shell.yy.o: shell.l
	$(LEX) shell.l
	$(CXX) -c lex.yy.c -o shell.yy.o
	$(RM) -f lex.yy.c

shell.o exec_cmd.o: $(@:.o=.cc)
	$(CXX) -c $(@:.o=.cc)

clean:
	-$(RM) -f $(testcases) mini-shell *.o *.tab.*

