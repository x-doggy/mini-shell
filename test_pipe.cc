#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using std::cout;
using std::endl;

int main() {

  pid_t   child_pid;
  int     fifo[2], status;
  char    buf[80];

  if (pipe(fifo) == -1) {
    perror("pipe");
    exit(1);
  }

  switch (child_pid = fork()) {
    case -1: perror("fork"); exit(2);
    case 0:
      close(fifo[0]); /* child process */
      write(fifo[1],"Child %d executed\n", getpid());
      close(fifo[1]);
      exit(0);
  }

  close(fifo[1]);    /* parent process */

  while (read(fifo[0], buf, 80)) {
    cout << buf << endl;
  }

  close(fifo[0]);

  if (waitpid(child_pid, &status, 0) == child_pid && WIFEXITED(status)) {
    return WEXITSTATUS(status);
  }

  return 0;
}
