#include <iostream>
#include <sys/types.h>
#include <unistd.h>

using std::cout;
using std::endl;

int main() {
  cout << "Process PID: " << getpid() << endl;
  cout << "Process PPID: " << getppid() << endl;
  cout << "Process PGID: " << getpgrp() << endl;
  cout << "Process real-UID: " << getuid() << endl;
  cout << "Process real-GID: " << getgid() << endl;
  cout << "Process effective-UID: " << geteuid() << endl;
  cout << "Process effective-GID: " << getegid() << endl;
  return 0;
}

